#!/bin/sh

set -e

avahi_install() {
	if [ -d /etc/avahi/services/ -a ! -e /etc/avahi/services/phpsysinfo.service -a ! -L /etc/avahi/services/phpsysinfo.service ] ; then
		ln -s ../../phpsysinfo/avahi.service /etc/avahi/services/phpsysinfo.service
	fi
}

lighttpd_install() {
	if [ ! -f /etc/lighttpd/conf-available/50-phpsysinfo.conf ] ; then
		if which lighty-enable-mod >/dev/null 2>&1 ; then
			ln -s ../../phpsysinfo/lighttpd.conf /etc/lighttpd/conf-available/50-phpsysinfo.conf
			lighty-enable-mod phpsysinfo fastcgi fastcgi-php || true
			avahi_install
		else
			echo "Lighttpd not installed, skipping"
		fi
	fi
}

nginx_install() {
	if [ ! -f /etc/nginx/sites-available/50-phpsysinfo.conf ] ; then
		if which nginx >/dev/null 2>&1 ; then
			echo '---------------------------------------------------------------------------------'
			echo 'Please add "include /etc/phpsysinfo/nginx.conf;" **inside** a nginx server block.'
			echo "For example in '/etc/nginx/sites-enabled/default'."
			echo "You may need to adjust the socket path in '/etc/phpsysinfo/nginx.conf'"
			echo "to another path like '/run/php/php8.2-fpm.sock'."
			echo 'Be sure to restart the webserver afterwards.'
			echo '---------------------------------------------------------------------------------'
			avahi_install
		else
			echo "nginx not installed, skipping"
		fi
	fi
}

apache2_install() {
	mkdir -p /etc/apache2/conf-available
	ln -sf ../../phpsysinfo/apache.conf /etc/apache2/conf-available/50-phpsysinfo.conf

	COMMON_STATE=$(dpkg-query -f '${Status}' -W 'apache2-data' 2>/dev/null | awk '{print $3}' || true)

	if [ -e /usr/share/apache2/apache2-maintscript-helper ] ; then
		. /usr/share/apache2/apache2-maintscript-helper
		apache2_invoke enconf 50-phpsysinfo
		avahi_install
	elif [ "$COMMON_STATE" = "installed" ] || [ "$COMMON_STATE" = "unpacked" ] ; then
		if [ -d /etc/apache2/conf.d/ ] && [ ! -L /etc/apache2/conf.d/50-phpsysinfo.conf ] ; then
			ln -s ../conf-available/50-phpsysinfo.conf /etc/apache2/conf.d/50-phpsysinfo.conf
		fi
	fi
}

if [ "$1" = "configure" ]; then

	# source debconf stuff
	if [ -f /usr/share/debconf/confmodule ]; then
		. /usr/share/debconf/confmodule
	fi

	# webserver configuration
	db_get phpsysinfo/reconfigure-webserver
	webservers="$RET"

	for webserver in $webservers; do
		webserver=${webserver%,}
		if [ "$webserver" = "lighttpd" ]; then
			lighttpd_install
		elif [ "$webserver" = "apache2" ]; then
			# Need to pass params for apache2-maintscript-helper
			apache2_install $@
		elif [ "$webserver" = "nginx" ]; then
			nginx_install
		fi
	done

	db_get phpsysinfo/restart-webserver
	res="$RET"
	if [ "$res" = "true" ]; then
		for webserver in $restart; do
			webserver=${webserver%,}

			if [ "$webserver" = "nginx" ]; then
				# The user needs to do some manual action
				continue
			fi

			# Redirection of 3 is needed because Debconf uses it and it might
			# be inherited by webserver. See bug #446324.
			if pathfind invoke-rc.d; then
				invoke-rc.d $webserver reload 3>/dev/null || true
			else
				/etc/init.d/$webserver reload 3>/dev/null || true
			fi
		done
	fi
fi

#DEBHELPER#

exit 0
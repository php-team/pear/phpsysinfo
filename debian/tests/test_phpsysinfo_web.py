import unittest

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
import os
import sys

class TestPhpSysinfoWeb(unittest.TestCase):

    def setUp(self):
        self.data_dir = os.getenv('AUTOPKGTEST_TMP', default='/tmp') + '/google-chrome'

        # print('Using chrome data dir: ' + self.data_dir)

        options = webdriver.ChromeOptions()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--headless')
        options.add_argument('--disable-gpu')
        options.add_argument('--disable-dev-shm-usage')
        options.add_argument('--profile-directory=Default')
        options.add_argument('--user-data-dir=' + self.data_dir)
        service = Service(executable_path=r'/usr/bin/chromedriver')
        self.driver = webdriver.Chrome(service=service, options=options)

    def tearDown(self):
        self.driver.close()

    def test_static_page(self):
        self.driver.get("http://localhost:8888/phpsysinfo/?disp=static")
        self.assertIn("phpSysInfo", self.driver.title)

        vitals = WebDriverWait(self.driver, 60).until(lambda x: x.find_element(By.ID, "vitals"))

        self.assertIn("OS Type", vitals.text)
        self.assertIn("Uptime", vitals.text)
        self.assertIn("Processes", vitals.text)

        hardware = WebDriverWait(self.driver, 60).until(lambda x: x.find_element(By.ID, "hardware"))

        self.assertIn("Processor", hardware.text)

        footer = WebDriverWait(self.driver, 60).until(lambda x: x.find_element(By.ID, "footer"))
        footer_link = footer.find_element(By.TAG_NAME, "a")

        self.assertIn("phpSysInfo - 3.", footer_link.text)

    def test_dynamic_page(self):
        self.driver.get("http://localhost:8888/phpsysinfo/?disp=dynamic")
        self.assertIn("phpSysInfo", self.driver.title)

        WebDriverWait(self.driver, 60).until(lambda x: x.find_element(By.ID, "s_processes").text != "")
        assert "System information:" in self.driver.title

        language = WebDriverWait(self.driver, 60).until(lambda x: x.find_element(By.ID, "language"))
        language = Select(language)
        language.select_by_value("en")


        vitals = WebDriverWait(self.driver, 60).until(lambda x: x.find_element(By.ID, "vitals"))

        self.assertIn("OS Type", vitals.text)
        self.assertIn("Uptime", vitals.text)
        self.assertIn("Processes", vitals.text)

        hardware = WebDriverWait(self.driver, 60).until(lambda x: x.find_element(By.ID, "hardware"))

        self.assertIn("Machine", hardware.text)
        self.assertIn("Processors", hardware.text)

        footer = WebDriverWait(self.driver, 60).until(lambda x: x.find_element(By.ID, "footer"))
        footer_link = footer.find_element(By.TAG_NAME, "a")

        self.assertIn("phpSysInfo - 3.", footer_link.text)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestPhpSysinfoWeb)
    result = unittest.TextTestRunner(verbosity=2).run(suite)

    if not result.wasSuccessful():
        sys.exit(1)

<?php

declare(strict_types = 1);

use \PHPUnit\Framework\TestCase;

class WebpageXSLTTest extends TestCase
{

    public function testOutput(): void
    {
        $webpage = new WebpageXSLT();
        ob_start();
        $webpage->run();
        $out = ob_get_clean();
        $xml = new DOMDocument();
        $xml->loadHTML($out);
        $this->assertEmpty(libxml_get_errors());
        $this->assertStringContainsString('Created by phpSysInfo - 3.', $xml->textContent);
        $this->assertStringContainsString('Load Averages', $xml->textContent);
    }
}
